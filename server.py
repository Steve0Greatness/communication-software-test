from flask import Flask, render_template as RenderTemplate, request
from json import dumps

app = Flask(__name__)

app.template_folder = "Client/templates"
app.static_folder = "Client/static"

INSTANCE_URL = "https://com.example.com"

@app.route("/")
def Index():
    return RenderTemplate("index.html")

@app.route("/.well-known/webfinger")
def WebFinger():
    Resource = request.args.get("resource")
    Info = {
        "subject": Resource,
        "aliases": [
            f"https://{INSTANCE_URL}/message/@",
            f"https://{INSTANCE_URL}/profile/@"
        ],
        "links": [
            {
                "rel": "http://webfinger.net/rel/profile-page",
                "type": "text/html",
                "href": "https://{INSTANCE_URL}/profile/@"
            }
        ]
    }
    return dumps(Info)

if __name__ == "__main__":
    app.run("0.0.0.0", 9000, debug=True)